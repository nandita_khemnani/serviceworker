var http = require('http');
var fs=require('fs');
var express = require('express');
var app = new express();

app.use(express.static(__dirname + '/public'));
app.listen(3027);
console.log('App started');

app.get("/", function (req,res) {
    res.sendFile(__dirname + "/index.html")

})
