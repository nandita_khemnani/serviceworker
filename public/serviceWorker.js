var cacheName = 'Cache2';
var cacheFiles = [
    '/',
    './staticFiles/about.html',
    './staticFiles/contact.html',
    './staticFiles/offline.html'
];

//installing service-worker
self.addEventListener('install', function (event) {
    console.log("Service Worker installed");
    event.waitUntil(
        caches.open(cacheName)
            .then(function (cache) {
                //service-worker caches these files
                return cache.addAll(cacheFiles);
            })
    );
});

self.addEventListener('activate', function (event) {
    console.log("Service Worker activated");

});

self.addEventListener('fetch', function (event) {

    event.respondWith(
        caches.match(event.request).then(function(response) {
            return response;
        })
    );
    event.waitUntil(
        update(event.request)
            .then(refresh)
    );
});
function update(request) {
    return caches.open(cacheName)
        .then(function (cache) {
            return fetch(request)
                .then(function (response) {
                    return cache.put(request, response.clone())
                        .then(function () {
                            return response;
                        });
                });
        });
}
